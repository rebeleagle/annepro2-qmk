#include <stdint.h>
#include "annepro2.h"
#include "qmk_ap2_led.h"

#define __ KC_TRNS
#define XX KC_NO
#define RAISE(keycode) LT(_RAISE,(keycode))
#define LOWER(keycode) LT(_LOWER,(keycode))
#define ADJ(keycode) LT(_ADJ,(keycode))

#define BT1 KC_AP2_BT1
#define BT2 KC_AP2_BT2
#define BT3 KC_AP2_BT3
#define BT4 KC_AP2_BT4

#define LED_OFF KC_AP_LED_OFF
#define LED_ON KC_AP_LED_ON
#define LED_INT KC_AP_LED_NEXT_INTENSITY
#define LED_SPEE KC_AP_LED_SPEED

#define M_L_P LCTL_T( KC_A )
#define M_L_R LALT_T( KC_O )
#define M_L_M LGUI_T( KC_E )
#define M_L_I LSFT_T( KC_U )
#define M_R_I RSFT_T( KC_H )
#define M_R_M RGUI_T( KC_T )
#define M_R_R LALT_T( KC_N )
#define M_R_P LCTL_T( KC_S )

#define L_L_I LOWER(KC_I)
#define L_R_I LOWER(KC_D)

#define M_WRKL LGUI(LCTL(KC_LEFT))
#define M_WRKR RGUI(RCTL(KC_RIGHT))

#define UK_PND UC(0x00A3)

// This is ROW*MATRIX_COLS + COL
#define CAPS_LOCATION (MATRIX_COLS * 2 + 0)

enum anne_pro_layers { _DV, _QW, _RAISE, _LOWER, _ADJ};

enum macros{
  M_TERM = AP2_SAFE_RANGE,
  M_DBL_SH,
  M_VI_SAV,
  SP_RAISE
};

enum tap_dances {
  T_PL_C,
  T_RL_A,
  T_ML_G,
  T_IL_S,
  T_IR_S,
  T_MR_G,
  T_RR_A,
  T_PR_C,
};
// Tap Dance definitions
qk_tap_dance_action_t tap_dance_actions[] = {
/*     [ TD_LSFT_CAPS ] = ACTION_TAP_DANCE_DOUBLE(KC_LSFT, KC_CAPS), */
/*     [ TD_RSFT_CAPS ] = ACTION_TAP_DANCE_DOUBLE(KC_RSFT, KC_CAPS), */
/*     Home Row Modifier keys */
    [ T_PL_C ] = ACTION_TAP_DANCE_DOUBLE(KC_A, KC_LCTL),
    [ T_RL_A ] = ACTION_TAP_DANCE_DOUBLE(KC_O, KC_LALT),
    [ T_ML_G ] = ACTION_TAP_DANCE_DOUBLE(KC_E, KC_LGUI),
    [ T_IL_S ] = ACTION_TAP_DANCE_DOUBLE(KC_U, KC_LSFT),
    [ T_IR_S ] = ACTION_TAP_DANCE_DOUBLE(KC_H, KC_LSFT),
    [ T_MR_G ] = ACTION_TAP_DANCE_DOUBLE(KC_T, KC_LGUI),
    [ T_RR_A ] = ACTION_TAP_DANCE_DOUBLE(KC_N, KC_LALT),
    [ T_PR_C ] = ACTION_TAP_DANCE_DOUBLE(KC_S, KC_LCTL),
  /* [ T_SPC_L2 ] = ACTION_TAP_DANCE_LAYER_MOVE( KC_SPC, _LOWER ), */
};

#define TDPL TD(T_PL_C)
#define TDRL TD(T_RL_A)
#define TDML TD(T_ML_G)
#define TDIL TD(T_IL_S)
#define TDIR TD(T_IR_S)
#define TDMR TD(T_MR_G)
#define TDRR TD(T_RR_A)
#define TDPR TD(T_PR_C)

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case M_TERM:
      if (record->event.pressed) {
        SEND_STRING(SS_DOWN(X_LALT)SS_DOWN(X_LGUI)"t"SS_UP(X_LALT)SS_UP(X_LGUI));
      }
      return false;
      break;
    case M_DBL_SH:
      if (record->event.pressed) {
        SEND_STRING(SS_TAP(X_LSFT) SS_TAP(X_LSFT));
      }
      return false;
      break;
    case M_VI_SAV:
      if (record->event.pressed) {
        SEND_STRING(":w"SS_TAP(X_ENT));
      }
      return false;
      break;
  }
  return true;
}

void matrix_init_user(void) {}
void matrix_scan_user(void) {}

const uint16_t keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  [_DV] = KEYMAP(
    XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX,
    ADJ(KC_TAB), KC_QUOT, KC_COMM, KC_DOT, KC_P, KC_Y, KC_F, KC_G, KC_C, KC_R, KC_L, KC_SLSH, KC_EQL, ADJ(KC_BSPC),
    KC_ESC, M_L_P, M_L_R, M_L_M, M_L_I, L_L_I, L_R_I, M_R_I, M_R_M, M_R_R, M_R_P, KC_MINS, KC_ENT,
    KC_CAPS, KC_SCLN, KC_Q, KC_J, KC_K, KC_X, KC_B, KC_M, KC_W, KC_V, KC_Z, TG(_RAISE),
    KC_DEL, TG(_LOWER), TT(_RAISE), LT(_RAISE, KC_SPC), KC_RCTL, KC_LALT, KC_RGUI, KC_RSFT
    ),

  [_QW] = KEYMAP(
    KC_ESC, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_MINS, KC_EQL, KC_BSPC,
    KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_LBRC, KC_RBRC, KC_BSLS,
    KC_CAPS, KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_QUOT, KC_ENT,
    KC_LSFT, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, RSFT_T(KC_UP),
    KC_LCTL, KC_LGUI, KC_LALT, LT(_ADJ,KC_SPC), KC_RALT, LT(_RAISE,KC_LEFT), LT(_LOWER,KC_DOWN), RCTL_T(KC_RGHT)
    ),

  /* Shifted Characters */
  /* [_RAISE] = KEYMAP( */
    /* XX, XX      , XX  , XX  , XX  , XX    , XX     , XX     , XX     , XX     , XX     , XX     , XX, XX, */
    /* __, KC_1    , KC_2, KC_3, KC_4, KC_5  , KC_PERC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_GRV , __, __, */
    /* __, KC_6    , KC_7, KC_8, KC_9, KC_0  , KC_CIRC, KC_EXLM, KC_AT  , KC_HASH, KC_DLR , KC_TILD, __, */
    /* __, M_VI_SAV, __  , __  , __  , UK_PND, KC_BSLS, KC_LCBR, KC_RCBR, KC_LBRC, KC_RBRC, TG(_RAISE), */
    /* __, __      , __  , __  , __  , __    , __     , __ */
    /* ) , */
  [_RAISE] = KEYMAP(
    XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX,
    __, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_GRV, UK_PND, __,
    __, KC_EXLM, KC_AT, KC_HASH, KC_DLR, KC_PERC, KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_TILD, __,
    __, M_VI_SAV, __, KC_LBRC, KC_RBRC, __, KC_PIPE, KC_LCBR, KC_RCBR, KC_BSLS, __, TG(_RAISE),
    __, __, __, __, __, __, __, __
    ),

  /* Fn Keys and Navigation */
  [_LOWER] = KEYMAP(
    XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX,
    M_WRKL, KC_F1, KC_F2 , KC_F3 , KC_F4 , __    , KC_HOME, KC_PGDN, KC_PGUP, KC_END , __    , __, __, M_WRKR,
    __    , KC_F5, KC_F6 , KC_F7 , KC_F8 , M_TERM, KC_LEFT, KC_DOWN, KC_UP  , KC_RGHT, KC_DEL, __, __,
    __    , KC_F9, KC_F10, KC_F11, KC_F12, M_WRKL, M_WRKR , KC_VOLD, KC_VOLU, __     , __    , __,
    __    , __   , __    , __    , __    , __    , __     , __
    )     ,

  [_ADJ] = KEYMAP(
    __, __ , __ , __     , __      , __, __, __     , __     , __, __, __, __, XX,
    __, BT1, BT2, BT3    , BT4     , __, __, DF(_QW), DF(_DV), __, __, __, __, __   ,
    __, __ , __ , LED_OFF, LED_ON  , __, __, __     , __     , __, __, __, __,
    __, __ , __ , LED_INT, LED_SPEE, __, __, __     , __     , __, __, RESET,
    __, __ , __ , __     , __      , __, __, __
    ) ,
};
const uint16_t keymaps_size = sizeof(keymaps);

void keyboard_post_init_user(void) {
  annepro2LedEnable();
  /* annepro2LedSetProfile(7); */
  annepro2LedSetForegroundColor(0x00, 0x00, 0x00);
}

layer_state_t layer_state_set_user(layer_state_t layer) {
  switch(get_highest_layer(layer)) {
    case _RAISE:
      // Set the leds to green
      annepro2LedSetForegroundColor(0x00, 0xFF, 0x00);
      break;
    case _LOWER:
      // Set the leds to blue
      annepro2LedSetForegroundColor(0x00, 0x00, 0xFF);
      break;
    default:
      // Reset back to the current profile
      /* annepro2LedSetProfile(7); */
      /* annepro2LedResetForegroundColor(); */
      annepro2LedSetForegroundColor(0x00, 0x00, 0x00);
      break;
  }
  return layer;
}

// The function to handle the caps lock logic
bool led_update_user(led_t leds) {
  if (leds.caps_lock) {
    // Set the leds to red
    annepro2LedSetForegroundColor(0xFF, 0x00, 0x00);
  } else {
    // Reset back to the current profile if there is no layer active
    if(!layer_state_is(_LOWER) && !layer_state_is(_RAISE)) {
      annepro2LedResetForegroundColor();
    }
  }

  return true;
}

